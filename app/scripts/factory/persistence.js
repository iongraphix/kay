app.factory("persistence", [
	"$cookies", function($cookies) {
		
		var id = 0;
		var name = "";
		var email = "";
		var uuid = "";

		return {
			setUser: function(id,name,email,uuid) {
				name = name;
				email = email;
				id = id;
				uuid = uuid;

				$cookies.put("id", id);
				$cookies.put("name", name);
				$cookies.put("email", email);
				$cookies.put("uuid", uuid);
			},
			getUser: function() {
				id = $cookies.get("id");
				name = $cookies.get("name");
				email = $cookies.get("email");
				uuid = $cookies.get("uuid");
				return { 'name': name, 'email' : email, 'id': id, 'uuid': uuid};
			},
			clearUser: function() {
				id = "";
				name = "";
				email = "";
				uuid  = "";
				
				$cookies.remove("id");
				$cookies.remove("name");
				$cookies.remove("email");
				$cookies.remove("uuid");
					
			}
		}
	}
]);
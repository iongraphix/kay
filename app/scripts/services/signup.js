app.service('register', function ($http){
    var host = $("body").data("url");
    var webservice_url = "http://"+host+"/resource/user";
    
    this.tryRegistration = function(name,email,password){
        return $http.post(webservice_url, { 'name': name,'email': email, 'password': password});
    };
});
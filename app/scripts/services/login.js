app.service('auth', function ($http){
    var host = $("body").data("url");
    var webservice_url = "http://"+host+"/auth";
    
    this.tryAuth = function(email,password){
        return $http.post(webservice_url, { 'email': email, 'password': password});
    };
});
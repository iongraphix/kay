app.controller('LoginController', function($scope,$cookies,$mdToast,$state,$rootScope,auth,$localStorage,persistence) {
    
    $rootScope.user.name = "";
    $rootScope.user.email = "";
    $scope.user.password = "";

    $scope.showPassword = function(e){
    	var email = $scope.user.email;
    	var password = $scope.user.password;

            auth.tryAuth(email,password).then(function(res){
                
                var data = res.data;
                if(data.status == "success"){
                    console.log("Showing Info");
                    console.dir(data.info);
                    
                    var role = data.info.role.title;
                    $rootScope.user = data.info;

                    if(role == "Student"){
                        $cookies.put("told", false);
                        $cookies.put("logged_in", "yes");
                        persistence.setUser(data.info.id, data.info.name, data.info.email, data.info.uuid);
                        $state.go('home.dashboard');
                    }
                }
                else if(data.status == "error"){
                    $mdToast.showSimple('Sorry, those credentials don\'t work.');
                }
            }, function(res){
                console.log("An Error Occurred!");
                $("body").html(res.data);
                $mdToast.showSimple('An Error Occurred, Check your internet connection');
                console.dir(res);
            });    
        
    };
});
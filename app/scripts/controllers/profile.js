app.controller('ProfileController', function($scope,$cookies,$mdToast,$mdDialog,$state,$rootScope,$localStorage,persistence,srv_users) {
    console.log("Look into my eyes");

    if($cookies.get("logged_in") === undefined){
        $state.go("access.signin");
        $mdToast.showSimple('Not so fast ;)');
   	}

    $scope.details  = persistence.getUser();
    
    $scope.$watch('details', function(newValue, oldValue){
         $rootScope.user = newValue;
      }, true);

    srv_users.getSingle($scope.details).then(function(res){
    	console.log("Got Sinle");
    	var data = res.data;
    	$scope.details = data;
    },function(res){

    });

    $scope.update = function(){
    	srv_users.editUser($scope.details).then(function(res){
    		$mdToast.showSimple('Details Updated Successfully');
    		$scope.details.password = "";
    	}, function(){

    	});
    };

    console.dir(persistence.getUser());

     $scope.showAdvanced = function(ev) {
		    $mdDialog.show({
		      controller: DialogController,
		      templateUrl: 'views/pages/tmpl/profile-image.tmpl.html',
		      targetEvent: ev,
		    })
		    .then(function(answer) {
		    	if(answer != "cancel"){
			    		srv_users.uploadImage({ 'id' : $scope.details.id, 'image' : answer } ).then(function(res){
			    		console.log("Uploaded");
			    		$scope.details.thumbnail = answer;
			    		console.dir(res);
				    	},function(res){

				    	});
		    	}
		    }, function() {
		      $scope.alert = 'You cancelled the dialog.';
		    });
  	};

  	var handleFileSelect=function(evt) {
      var file=evt.currentTarget.files[0];
      var reader = new FileReader();
      reader.onload = function (evt) {
        $scope.$apply(function($scope){
          $rootScope.myImage=evt.target.result;
          $scope.myImage = evt.target.result;
          $scope.showAdvanced();
        });
      };

      if(document.getElementById("fileInput").files.length != 0){
      	reader.readAsDataURL(file);
      }
    };

    angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);

});

function DialogController($scope, $mdDialog,$rootScope,$mdToast) {
  $scope.myImage = $rootScope.myImage;
  $scope.myCroppedImage='';
  $scope.cropType="circle";

  $scope.hide = function() {
    $mdDialog.hide();
  };

  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  $scope.answer = function(answer) {
  	if(answer == "cancel"){
  		$mdDialog.hide(answer);
  	}
  	else if(answer == "upload"){
  		$mdDialog.hide($scope.myCroppedImage);
  	}
  };
};
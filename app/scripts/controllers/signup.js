app.controller('SignupController', function($scope,$mdToast,$state,$rootScope,register) {

    $rootScope.user.name = "";
    $rootScope.user.email = "";
    $scope.user.password = "";

    $scope.doRegister = function(){

        var fullname = $scope.user.name;
        var email = $scope.user.email;
        var password = $scope.user.password;

        register.tryRegistration(fullname, email, password).then(function(res){

            var data = res.data;
            console.dir(data);
            if(data.status == "success"){
                console.dir(data);
                $rootScope.name = data.info.name;
                $rootScope.email = data.info.email;
                $rootScope.newuser = true;
                $mdToast.showSimple('You registration was successful. Login to continue');
                $state.go('access.signin');
            }

        },function(res){
            $("body").html(res.data);
            console.log(res);
        });
    };
});
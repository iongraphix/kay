
UECO Cloud Angular JS Web Client
================================
This is the AngularJS based web client for the UECO Cloud  Web Service

Installation
--------------
Copy `rest.api.example` to `rest.api` and modify the file accordingly.

>Start your browser and enjoy
> Written with [StackEdit](https://stackedit.io/).
